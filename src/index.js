import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import App from './App';
import About from './About'
import UserProfile from './UserProfile'
import Register from './Register'
import Signup from './Signup'
import * as serviceWorker from './serviceWorker';
import "bootstrap/dist/css/bootstrap.min.css";
import logo from "./logo.svg"


const routing = (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          <img style={{height:"100px", width:"100px"}} src={logo} />
          Belajar React
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
  
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="navbar-brand" to="/">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/UserProfile">
                Profile
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/about">
                About
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/Register">
                register
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/Signup">
                Sign Up
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      <Route exact path="/" component={App} />
      <Route path="/about/:number?" component={About} />
      <Route path="/UserProfile/:user?" component={UserProfile} />
      <Route path="/Register" component={Register}/>
      <Route path="/Signup" component={Signup}/>
    </Router>
  );
  


ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
