// import React from 'react';

// import { Container } from './styles';

// class MultiInputForm extends React.Component {
//     constructor(props) {
//       super(props);
//       this.state = {
//         isGoing: true,
//         numberOfGuests: 2
//       };

//       this.handleInputChange = this.handleInputChange.bind(this);
//       this.handleSubmit = this.handleSubmit.bind(this);
//     }

//     handleInputChange(event) {
//       const target = event.target;
//       const value = target.type === 'checkbox' ? target.checked : target.value;
//       const name = target.name;

//       this.setState({
//         [name]: value
//       });
//     }

//     handleSubmit(event) {
//         alert('Is going: ' + this.state.isGoing + ` \nNumber of guest ${this.state.numberOfGuests}` );
//         event.preventDefault();
//       }

//     render() {
//       return (
//         <form onSubmit={this.handleSubmit}>
//           <label>
//             Is going:
//             <input
//               name="isGoing"
//               type="checkbox"
//               checked={this.state.isGoing}
//               onChange={this.handleInputChange} />
//           </label>
//           <br />
//           <label>
//             Number of guests:
//             <input
//               name="numberOfGuests"
//               type="number"
//               value={this.state.numberOfGuests}
//               onChange={this.handleInputChange} />
//           </label>
//             <input type="submit" value="Submit" />
//         </form>
//       );
//     }
//   }

//   export default MultiInputForm






import React from "react";
import useForm from "react-hook-form";


export default function App() {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = data => console.log(data);
  console.log(errors);

  return (
    <form className="mx-5" onSubmit={handleSubmit(onSubmit)}>
      <br />
      <h1>Sign Up</h1>
      <br />
      <br />
      <div className="form-group">
        <label for="Input">Name :</label>
        <input
          type="text"
          placeholder="Name"
          name="Name"
          ref={register({ required: true, maxLength: 30 })}
        />
     
      </div>

      <div className="form-group">
        <label for="Input">Email :</label>
        <input 
          type="text"
          placeholder="Email"
          name="Email"
          ref={register({ required: true, pattern: /^\S+@\S+$/i })}
        /></div>

            <br/><br/>  
        <label for="Input">Password :</label>
        <input
          type="password"
          placeholder="Password"
          name="Password"
          ref={register({ required: true })}
        />
         <label for="Input">Confirm Password :</label>
        <input
          type="password"
          placeholder="Password"
          name="Password"
          ref={register({ required: true })}
        />
    
      <br />
      
        <input type="submit" value="Sign Up" className="btn btn-dark"/>
        <div style={{ color: 'red' }}>{Object.keys(errors).length > 0 && 'Name,Email,Password Belum Terisi.'}</div>
    </form>
  );
}
